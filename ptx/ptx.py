## Dependencies
##

# Python Standard
import re

class PretextProcessor:
    """
    This is the one and only class that implements the pretext text pre-processor

    Pretext was designed with simplicity and ease of use as a priority

    There are only two user methods
        process_infile()
        generate_outfile()
    """

    def __init__(self, infile, outfile):
        """
        """
        # Initialise substitution dictionary
        #
        # This is a list of re's used to find sub-strings in the NON-pretext lines
        # within the input file
        self.ptx_subst = {
            'subst'      : re.compile(r'\$\|([^\|]+)\|\$'), # Straight substitutin
            'subst_l'    : re.compile(r'\$\<([^\|]+)\|\$'), # Substitution with left padding [UNUSED]
            'subst_r'    : re.compile(r'\$\>([^\|]+)\|\$')  # Substitution with righ padding [UNUSED]
        }

        # Initialise line patterns dictionary
        #
        # This is a list of re's used to determine what type of line we are processing
        self.ptx_line = {
            'single_line': re.compile(r'^(\s*)//!px\s*(#.*)*\n*'),  # Single line pretext code
            'begin_line' : re.compile(r'^(\s*)/\*!px\s*(#.*)*\n*'), # Beginning of a pretext code block
            'end_line'   : re.compile(r'^(\s*)px!\*/\s*(#.*)*\n*'), # End of a pretext code block
            'line'       : re.compile(r'^(\s*)([^#:]+)(:)*\s*(#.*)*\n*') # NON-pretext line
        }

        # State values to determine the context of the current line
        self.ptx_annotation = False
        self.ptx_block_ended = False

        # Current indent levels required for generating valid python structure
        self.ptx_default_indent = "    "
        self.ptx_base_indent = "    "

        # The path to the input and output files
        self.ptx_infile = infile
        self.ptx_outfile = outfile

        # First line to be added to the python code object
        self.ptx_processed_file_data = ["with open(self.ptx_outfile, 'w') if self.ptx_outfile != '-' else sys.stdout as f:"]

    def _substitute (self, line):
        """
        Search for substitution strings within a line and substiture with literal values/expressions
        that are defined or derived from the embedded pretext code
        """
        sub_line = ""
        # Iterate through all the substitution strings
        for s in self.ptx_subst:
            # If it is present in the current line, make the substitute for value
            m = self.ptx_subst[s].sub(r'{\1}', line)

            # If one or more substitutions have been made return the updated line
            if m:
                sub_line = m
                return sub_line
        # Return the original line if no substitutions were made
        return line

    ## Process the input file contents to generate executable python to process the file
    ## contents and generte output
    ##
    def process_infile(self):
        """
        Takes an input file (any form of text file) containing embedded pretext code and
        processes it line by line.

        Non pretext code/annotation lines are printed directly into the output file with
        any variable/expressionsubstitutions resolved.

        Single lines or blocks of embedded pretext code are used to manipulate and control
        this process providing the full power of python and its control structures allowing
        repetition, conditional inclusion and variable expansion and more
        """

        with open(self.ptx_infile, 'r') if self.ptx_infile != "-" else sys.stdin as f:
            infile_lines = f.readlines()

            # Process line by line
            for line in infile_lines:
                m = None
                gen_line = ""

                stripped_line = line.rstrip()

                # If the previsous line was a ptx block end line
                if self.ptx_block_ended:
                    self.ptx_annotation = False
                    self.ptx_block_ended = False

                # Test for a ptx code block begin line
                m = self.ptx_line['begin_line'].match(stripped_line)
                if m:
                    self.ptx_annotation = True
                    self.ptx_base_indent = ""
                    gen_line = f"{self.ptx_default_indent}{m.group(2)}"
                else:
                    # Test for a ptx single code line
                    m = self.ptx_line['single_line'].match(stripped_line)
                    if m:
                        self.ptx_annotation = True
                        self.ptx_base_indent = ""
                        gen_line = f"{self.ptx_default_indent}{m.group(2)}"
                    else:
                        # Test for a ptx code block end line
                        m = self.ptx_line['end_line'].match(stripped_line)
                        line_indent = ""
                        if m:
                            self.ptx_annotation = True
                            self.ptx_block_ended = True
                            gen_line = f"{self.ptx_default_indent}{m.group(2)}"
                        else:
                            # Test for a NON-ptx mark line - normal file content or ptx code 
                            # within a ptx code block
                            m = self.ptx_line['line'].match(stripped_line)
                            if m:
                                # Is the current line with a ptx code block
                                if self.ptx_annotation:
                                    gen_line = f"{self.ptx_default_indent}{m.group(0)}"
                                    
                                    if m.group(1):
                                        line_indent = m.group(1)
                                    else: 
                                        line_indent = ""

                                    if m.group(3):
                                        self.ptx_base_indent = f"{self.ptx_default_indent}{line_indent}    "
                                    else:
                                        self.ptx_base_indent = f"{self.ptx_default_indent}{line_indent}"
                                # File content line requiring no processing other than to be printed
                                # into the output file
                                else:
                                    gen_line = f"{self.ptx_base_indent}f.write(f\"{m.group(0)}{m.group(1)}\\n\")"

                # Perform variable expansion/expression substitution
                sub_line = self.substitute(gen_line)

                # Append the processed line onto the output list
                self.ptx_processed_file_data.append(sub_line)

        # Return the output list containing the lines of the 'intermediate' file representation. This may either
        # be written out to a file and executed by python to generate the final fully processed output file, or,
        # converted into a code object by compile() and then executed by exec()
        return self.ptx_processed_file_data

    def generate_file(self):
        """
        From the processed input file lines 'ptx_processed_file_data', generate/compile a code object
        which may be executed using exec()
        """
        code = '\n'.join(self.ptx_processed_file_data)
        code_object = compile(code, 'generted-by-pretext', 'exec')
        exec(code_object)

